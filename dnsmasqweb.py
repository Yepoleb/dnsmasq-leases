from dataclasses import dataclass
import time
import pathlib
import re
import ipaddress
import socket

import flask
import pyroute2
import pyroute2.netlink.rtnl.ndmsg as ndmsg

LEASES_PATH = "/var/lib/misc/dnsmasq.leases"
CONFIG_ROOT = pathlib.Path("/etc/dnsmasq.d")

FILTER_FLAGS = (
    ndmsg.NUD_INCOMPLETE |
    ndmsg.NUD_FAILED |
    ndmsg.NUD_NOARP |
    ndmsg.NUD_PERMANENT
)

@dataclass
class DhcpLease:
    name: str
    ip: str
    mac: str
    expires: str

@dataclass
class StaticHost:
    name: str
    ip: str

@dataclass
class ArpEntry:
    name: str
    ip: str
    mac: str
    refs: int
    used: str


def process_neighbours(ipr_neigh, mac_mapping):
    neighbours = []
    for neigh_entry in ipr_neigh:
        attrs_dict = dict(neigh_entry["attrs"])
        if neigh_entry["state"] & FILTER_FLAGS:
            continue
        required_attrs = ("NDA_DST", "NDA_LLADDR", "NDA_CACHEINFO")
        if any(attr_name not in attrs_dict for attr_name in required_attrs):
            continue
        mac = attrs_dict["NDA_LLADDR"].lower()
        name = mac_mapping.get(mac, "")
        ip = attrs_dict["NDA_DST"]
        cacheinfo = attrs_dict["NDA_CACHEINFO"]
        refs = cacheinfo["ndm_refcnt"]
        used = "/".join(str(u) for u in (
            cacheinfo["ndm_used"] // 100,
            cacheinfo["ndm_confirmed"] // 100,
            cacheinfo["ndm_updated"] // 100
        ))
        neighbours.append(ArpEntry(name, ip, mac, refs, used))
    return neighbours

app = flask.Flask(__name__)

@app.route("/")
def index():
    leases = []
    mac_mapping = {}
    leases_f = open(LEASES_PATH)
    for line in leases_f:
        line = line.strip()
        if not line:
            continue
        expire_ts_str, mac, ip, name, client_id = line.split()

        expire_ts = int(expire_ts_str)
        if expire_ts != 0:
            now = int(round(time.time()))
            expires_seconds = expire_ts - now
            hours, remainder = divmod(expires_seconds, 3600)
            minutes, seconds = divmod(remainder, 60)
            expire_str = f"{hours:02}:{minutes:02}:{seconds:02}"
        else:
            expire_str = "Never"

        leases.append(DhcpLease(name, ip, mac, expire_str))
        mac_mapping[mac.lower()] = name
    leases_f.close()

    static_hosts = []
    for config_path in CONFIG_ROOT.iterdir():
        config_txt = open(config_path).read()
        dhcp_entries = re.findall(
            r"dhcp-host=([0-9a-fA-F:]+),([^,\n]+)(?:,([0-9a-fA-F.]+))?",
            config_txt
        )
        static_entries = re.findall(
            r"address=/([^/\n]+)/([^\n]+)",
            config_txt
        )
        for mac, name, ip in dhcp_entries:
            mac_mapping[mac.lower()] = name
            if ip:
                static_hosts.append(StaticHost(name, ip))
        for name, ip in static_entries:
            static_hosts.append(StaticHost(name, ip))

    ipr = pyroute2.IPRoute()
    neigh_v4 = process_neighbours(
        ipr.get_neighbours(socket.AF_INET),
        mac_mapping
    )
    neigh_v6 = process_neighbours(
        ipr.get_neighbours(socket.AF_INET6),
        mac_mapping
    )

    leases.sort(key=lambda e: e.name)
    # len(e.ip) is a hack to sort IPv4 first
    static_hosts.sort(key=lambda e: (e.name, len(e.ip)))
    neigh_v4.sort(key=lambda e: (e.name, e.ip))
    neigh_v6.sort(key=lambda e: (e.name, e.ip))

    return flask.render_template(
        "dnsmasqweb.html",
        leases=leases,
        static_hosts=static_hosts,
        neigh_v4=neigh_v4,
        neigh_v6=neigh_v6
    )
